// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IoC.cs" company="Web Advanced">
// Copyright 2012 Web Advanced (www.webadvanced.com)
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------


using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using GradAcademyCSharp.Models;
using GradAcademyCSharp.Service;
using GradAcademyCSharp.Service.Contracts;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using StructureMap.Web.Pipeline;

namespace GradAcademyCSharp.DependencyResolution {
	using StructureMap;
	
	public static class IoC {
		public static IContainer Initialize() {
			var container = new Container(c =>
			{
				c.AddRegistry<DefaultRegistry>();
				c.For<IControllerActivator>().Singleton().Use<StructureMapControllerActivator>();

				c.For<IUserStore<ApplicationUser>>()
					.Use<UserStore<ApplicationUser>>().LifecycleIs<HttpContextLifecycle>();
				c.For<IAuthenticationManager>().UseSpecial(x =>
					x.ConstructedBy(i => HttpContext.Current.GetOwinContext().Authentication));
				c.For<DbContext>().Use(() => new ApplicationDbContext()).LifecycleIs<HttpContextLifecycle>();

				c.For<IWorkerService>().Singleton().Singleton().Use<WorkerService>();
				c.For<IClientService>().Singleton().Use<ClientService>();
				c.For<IPaymentService>().Singleton().Use<PaymentService>();
			});

			container = Service.IoC.Configure(container);

			return container;
		}
	}
}