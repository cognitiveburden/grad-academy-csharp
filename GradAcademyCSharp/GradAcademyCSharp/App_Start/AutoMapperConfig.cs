﻿using AutoMapper;
using GradAcademyCSharp.BusinessObjects;
using GradAcademyCSharp.Models;
using GradAcademyCSharp.Service;

namespace GradAcademyCSharp.App_Start
{
    public class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.CreateMap<Worker, WorkerModel>().ForMember(x => x.NumChecksCompleteInLastSevenDays, opt => opt.Ignore()).ForMember(x => x.AverageCompletionTime, opt => opt.Ignore());
            Mapper.CreateMap<WorkerModel, Worker>();

            Mapper.CreateMap<Client, ClientModel>();
            Mapper.CreateMap<ClientModel, Client>().ForMember(x => x.State, opt => opt.Ignore());

            Mapper.CreateMap<Check, CheckModel>();
            Mapper.CreateMap<CheckModel, Check>();

            Mapper.CreateMap<Invoice, InvoiceModel>().ForMember(x => x.Scheduled, opt => opt.Ignore());
            Mapper.CreateMap<InvoiceModel, Invoice>();

            Mapper.CreateMap<PaidInvoice, PaidInvoiceModel>().ForMember(x => x.AmountDueAfter, opt => opt.Ignore());
            Mapper.CreateMap<PaidInvoiceModel, PaidInvoice>().ForMember(x => x.WorkerId, opt => opt.Ignore());

            AutoMapperServiceConfiguration.Configure();
        }
    }
}