﻿using System;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using GradAcademyCSharp.BusinessObjects;
using GradAcademyCSharp.Models;
using GradAcademyCSharp.Service.Contracts;

namespace GradAcademyCSharp.Controllers
{
    public class ClientController : Controller
    {
        private readonly IClientService _clientService;

        public ClientController(IClientService clientService)
        {
            _clientService = clientService;
        }

        // GET: Worker
        public ActionResult Index()
        {
            var workerModels = Enumerable.ToList(_clientService.GetClients().Select(Mapper.Map<Client, ClientModel>));

            return View(workerModels);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var clientModel = _clientService.GetClient(id);

            return View(Mapper.Map<ClientModel>(clientModel));
        }

        [HttpPost]
        public ActionResult Edit(ClientModel clientModel)
        {
            if (!ModelState.IsValid)
            {
                return View(clientModel);
            }

            States matchingState;
            var stateParsed = Enum.TryParse(clientModel.State.ToUpper(), out matchingState);

            if (!stateParsed)
            {
                ModelState.AddModelError("Invalid state", "Invalid state code.");
                return View(clientModel);
            }

            var client = Mapper.Map<Client>(clientModel);
            client.State = matchingState;

            var updatedModel = Mapper.Map<ClientModel>(_clientService.UpdateClientInformation(client.Id, client));

            return View("Details", updatedModel);
        }

        public ActionResult Details(int id)
        {
            return View(Mapper.Map<ClientModel>(_clientService.GetClient(id)));
        }
    }
}