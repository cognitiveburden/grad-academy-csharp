using System.ComponentModel.DataAnnotations;

namespace GradAcademyCSharp.Models
{
    public class ClientModel
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [StringLength(2)]
        public string State { get; set; }
    }
}