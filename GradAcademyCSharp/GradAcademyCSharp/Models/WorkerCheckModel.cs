﻿using System.Collections.Generic;

namespace GradAcademyCSharp.Models
{
    public class WorkerCheckModel
    {
        public WorkerModel Worker { get; set; }

        public IEnumerable<CheckModel> WorkableChecks { get; set; } 
    }
}