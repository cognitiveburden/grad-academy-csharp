﻿using System.Data.Entity;
using GradAcademyCSharp.DataAccess.Models;

namespace GradAcademyCSharp.DataAccess.Process
{
    public class FinancialsDataContext : DbContext
    {
        public FinancialsDataContext() : base("DefaultConnection") { }

        public DbSet<WorkerEntity> Workers { get; set; }

        public DbSet<ClientEntity> Clients { get; set; }

        public DbSet<CheckEntity> Checks { get; set; }

        public DbSet<InvoiceEntity> Invoices { get; set; }

        public DbSet<PaidInvoiceEntity> PaidInvoices { get; set; }
    }
}
