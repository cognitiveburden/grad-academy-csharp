﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using GradAcademyCSharp.BusinessObjects;
using GradAcademyCSharp.DataAccess.Models;
using GradAcademyCSharp.DataAccess.Process.Contracts;

namespace GradAcademyCSharp.DataAccess.Process
{
    public class WorkerDataAccess : IFinancialDataAccess<Worker>
    {
        public Worker Add(Worker newItem)
        {
            WorkerEntity newWorker;
            try
            {
                using (var context = new FinancialsDataContext())
                {
                    newWorker = context.Workers.Add(Mapper.Map<WorkerEntity>(newItem));
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                return null;
            }

            return Mapper.Map<WorkerEntity, Worker>(newWorker);
        }

        public bool Remove(int id)
        {
            try
            {
                using (var context = new FinancialsDataContext())
                {
                    var worker = context.Workers.FirstOrDefault(x => x.Id == id);
                    if (worker == null)
                        return false;

                    context.Workers.Remove(worker);
                    context.SaveChanges();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Worker Update(int id, Worker updatedItem)
        {
            try
            {
                using (var context = new FinancialsDataContext())
                {
                    var worker = context.Workers.FirstOrDefault(x => x.Id == id);
                    if (worker == null)
                        return null;

                    Mapper.Map(updatedItem, worker);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                return null;
            }

            return Get(id);
        }

        public Worker Get(int id)
        {
            using (var context = new FinancialsDataContext())
            {
                return Mapper.Map<Worker>(context.Workers.FirstOrDefault(x => x.Id == id));
            }
        }

        public IList<Worker> Get()
        {
            using (var context = new FinancialsDataContext())
            {
                return Enumerable.ToList(context.Workers.Select(Mapper.Map<Worker>));
            }
        }
    }
}
