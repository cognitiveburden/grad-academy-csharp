﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using AutoMapper;
using GradAcademyCSharp.BusinessObjects;
using GradAcademyCSharp.DataAccess.Models;
using GradAcademyCSharp.DataAccess.Process.Contracts;

namespace GradAcademyCSharp.DataAccess.Process
{
    public class ClientDataAccess : IFinancialDataAccess<Client>
    {
        public Client Add(Client newItem)
        {
            ClientEntity newClient;
            try
            {
                using (var context = new FinancialsDataContext())
                {
                    newClient = context.Clients.Add(Mapper.Map<ClientEntity>(newItem));
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.StackTrace);
                Trace.TraceError(ex.Message);

                return null;
            }

            return Mapper.Map<Client>(newClient);
        }

        public bool Remove(int id)
        {
            try
            {
                using (var context = new FinancialsDataContext())
                {
                    var client = context.Clients.FirstOrDefault(x => x.Id == id);
                    if (client == null)
                        return false;

                    context.Clients.Remove(client);
                    context.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.StackTrace);
                Trace.TraceError(ex.Message);

                return false;
            }
        }

        public Client Update(int id, Client updatedItem)
        {
            try
            {
                using (var context = new FinancialsDataContext())
                {
                    var client = context.Clients.FirstOrDefault(x => x.Id == id);
                    if (client == null)
                        return null;

                    Mapper.Map(updatedItem, client);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.StackTrace);
                Trace.TraceError(ex.Message);

                return null;
            }

            return updatedItem;
        }

        public Client Get(int id)
        {
            using (var context = new FinancialsDataContext())
            {
                return Mapper.Map<Client>(context.Clients.FirstOrDefault(x => x.Id == id));
            }
        }

        public IList<Client> Get()
        {
            using (var context = new FinancialsDataContext())
            {
                return context.Clients.Select(Mapper.Map<Client>).ToList();
            }
        }
    }
}