﻿using System.Collections.Generic;

namespace GradAcademyCSharp.DataAccess.Process.Contracts
{
    public interface IFinancialDataAccess<T>
    {
        T Add(T newItem);
        bool Remove(int id);
        T Update(int id, T updatedItem);
        T Get(int id);
        IList<T> Get();
    }
}
