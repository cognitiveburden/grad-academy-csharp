﻿using GradAcademyCSharp.BusinessObjects;

namespace GradAcademyCSharp.DataAccess.Process.Contracts
{
    public interface IPaymentDataAccess
    {
        bool Update(int checkId, int invoiceId, Check check, Invoice invoice);
    }
}