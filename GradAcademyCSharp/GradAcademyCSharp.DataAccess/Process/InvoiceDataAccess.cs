using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using GradAcademyCSharp.BusinessObjects;
using GradAcademyCSharp.DataAccess.Models;
using GradAcademyCSharp.DataAccess.Process.Contracts;

namespace GradAcademyCSharp.DataAccess.Process
{
    public class InvoiceDataAccess : IFinancialDataAccess<Invoice>
    {
        public Invoice Add(Invoice newItem)
        {
            InvoiceEntity newInvoiceEntity;
            try
            {
                using (var context = new FinancialsDataContext())
                {
                    newInvoiceEntity = context.Invoices.Add(Mapper.Map<InvoiceEntity>(newItem));
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                return null;
            }

            return Mapper.Map<Invoice>(newInvoiceEntity);
        }

        public bool Remove(int id)
        {
            try
            {
                using (var context = new FinancialsDataContext())
                {
                    var invoice = context.Invoices.FirstOrDefault(x => x.Id == id);
                    if (invoice == null)
                        return false;

                    context.Invoices.Remove(invoice);
                    context.SaveChanges();

                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Invoice Update(int id, Invoice updatedItem)
        {
            try
            {
                using (var context = new FinancialsDataContext())
                {
                    var invoice = context.Invoices.FirstOrDefault(x => x.Id == id);
                    if (invoice == null)
                        return null;

                    Mapper.Map(updatedItem, invoice);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                return null;
            }

            return Get(id);
        }

        public Invoice Get(int id)
        {
            using (var context = new FinancialsDataContext())
            {
                return Mapper.Map<Invoice>(context.Invoices.FirstOrDefault(x => x.Id == id));
            }
        }

        public IList<Invoice> Get()
        {
            using (var context = new FinancialsDataContext())
            {
                return Enumerable.ToList(context.Invoices.Select(Mapper.Map<Invoice>));
            }
        }
    }
}