﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using GradAcademyCSharp.BusinessObjects;
using GradAcademyCSharp.DataAccess.Models;
using GradAcademyCSharp.DataAccess.Process.Contracts;

namespace GradAcademyCSharp.DataAccess.Process
{
    public class PaidInvoiceDataAccess : IFinancialDataAccess<PaidInvoice>
    {
        public PaidInvoice Add(PaidInvoice newItem)
        {
            PaidInvoiceEntity newPaidInvoice;
            try
            {
                using (var context = new FinancialsDataContext())
                {
                    newPaidInvoice = context.PaidInvoices.Add(Mapper.Map<PaidInvoiceEntity>(newItem));
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                return null;
            }

            return Mapper.Map<PaidInvoiceEntity, PaidInvoice>(newPaidInvoice);
        }

        public bool Remove(int id)
        {
            try
            {
                using (var context = new FinancialsDataContext())
                {
                    var paidInvoice = context.PaidInvoices.FirstOrDefault(x => x.Id == id);
                    if (paidInvoice == null)
                        return false;

                    context.PaidInvoices.Remove(paidInvoice);
                    context.SaveChanges();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public PaidInvoice Update(int id, PaidInvoice updatedItem)
        {
            try
            {
                using (var context = new FinancialsDataContext())
                {
                    var paidInvoice = context.PaidInvoices.FirstOrDefault(x => x.Id == id);
                    if (paidInvoice == null)
                        return null;

                    Mapper.Map(updatedItem, paidInvoice);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                return null;
            }

            return Get(id);
        }

        public PaidInvoice Get(int id)
        {
            using (var context = new FinancialsDataContext())
            {
                return Mapper.Map<PaidInvoice>(context.PaidInvoices.FirstOrDefault(x => x.Id == id));
            }
        }

        public IList<PaidInvoice> Get()
        {
            using (var context = new FinancialsDataContext())
            {
                return context.PaidInvoices.Select(Mapper.Map<PaidInvoice>).ToList();
            }
        }
    }
}
