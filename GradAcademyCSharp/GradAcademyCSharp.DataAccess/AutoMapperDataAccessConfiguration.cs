﻿using GradAcademyCSharp.BusinessObjects;
using GradAcademyCSharp.DataAccess.Models;

namespace GradAcademyCSharp.DataAccess
{
    public class AutoMapperDataAccessConfiguration
    {
        public static void Configure()
        {
            AutoMapper.Mapper.CreateMap<WorkerEntity, Worker>();
            AutoMapper.Mapper.CreateMap<Worker, WorkerEntity>();

            AutoMapper.Mapper.CreateMap<ClientEntity, Client>();
            AutoMapper.Mapper.CreateMap<Client, ClientEntity>();

            AutoMapper.Mapper.CreateMap<CheckEntity, Check>().ForMember(x => x.ClientName, opt => opt.Ignore());
            AutoMapper.Mapper.CreateMap<Check, CheckEntity>().ForMember(x => x.ClientEntity, opt => opt.Ignore());

            AutoMapper.Mapper.CreateMap<InvoiceEntity, Invoice>();
            AutoMapper.Mapper.CreateMap<Invoice, InvoiceEntity>().ForMember(x => x.ClientEntity, opt => opt.Ignore());

            AutoMapper.Mapper.CreateMap<PaidInvoiceEntity, PaidInvoice>();
            AutoMapper.Mapper.CreateMap<PaidInvoice, PaidInvoiceEntity>().ForMember(x => x.InvoiceEntity, opt => opt.Ignore()).ForMember(x => x.WorkerEntity, opt => opt.Ignore());

            AutoMapper.Mapper.AssertConfigurationIsValid();

        }
    }
}
