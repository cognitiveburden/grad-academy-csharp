﻿using System.ComponentModel.DataAnnotations;

namespace GradAcademyCSharp.DataAccess.Models
{
    public class WorkerEntity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public string JobTitle { get; set; }

        [EmailAddress]
        [Required]
        public string EmailAddress { get; set; }

    }
}
