﻿using System.ComponentModel.DataAnnotations;
using GradAcademyCSharp.BusinessObjects;

namespace GradAcademyCSharp.DataAccess.Models
{
    public class ClientEntity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public States State { get; set; }
    }
}
