﻿using System.Collections.Generic;
using System.IO;
using System.Net.NetworkInformation;
using CsvHelper;
using Faker;
using GradAcademyLoader.Domain;

namespace GenerateTestData
{
	public static class GenerateCsvDataToFile
	{
		public static void Generate(int divisorForBadData = 1)
		{
			var dataBuilder = new DataBuilder(new GenerateRandomClient(divisorForBadData), new GenerateRandomCheck(divisorForBadData));

			var data = dataBuilder.GenerateResults(20, 2);
			var config = new LoaderConfig();

			PrintDataToCsv(data, config);
		}

		private static void PrintDataToCsv(Data data, LoaderConfig config)
		{
			var clientsPath = Path.Combine(Path.GetTempPath(), config.CsvClientsFileName);
			var checksPath = Path.Combine(Path.GetTempPath(), config.CsvChecksFileName);

			Print(data.Clients, clientsPath);
			Print(data.Checks, checksPath);
		}

		private static void Print<T>(IEnumerable<T> items, string path)
		{
			if (File.Exists(path))
				File.Delete(path);

			using (var textWriter = new StreamWriter(path))
			{
				var csv = new CsvWriter(textWriter);
				csv.WriteRecords(items);
			}

		}

	}
}
