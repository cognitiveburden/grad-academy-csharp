using System.Collections.Generic;
using System.IO;
using GradAcademyLoader.Domain;
using Newtonsoft.Json;

namespace GenerateTestData
{
	public static class GenerateJsonDataToFile
	{
		public static void Generate(int divisorForBadData = 1)
		{
			var dataBuilder = new DataBuilder(new GenerateRandomClient(divisorForBadData), new GenerateRandomCheck(divisorForBadData));
			var data = dataBuilder.GenerateResults(20, 3);

			var config = new LoaderConfig();

			PrintDataToJson(data,config);
		}

		private static void PrintDataToJson(Data data, LoaderConfig config)
		{
			var clientsPath = Path.Combine(Path.GetTempPath(), config.JsonClientsFileName);
			var checksPath = Path.Combine(Path.GetTempPath(), config.JsonChecksFileName);

			Print(data.Clients, clientsPath);
			Print(data.Checks, checksPath);
		}

		private static void Print<T>(IEnumerable<T> items, string path)
		{
			if (File.Exists(path))
				File.Delete(path);

			string json = JsonConvert.SerializeObject(items, Formatting.Indented, new JsonSerializerSettings(){Converters = new List<JsonConverter>(){new Newtonsoft.Json.Converters.StringEnumConverter()}});

			using (var textWriter = new StreamWriter(path))
			{
				textWriter.Write(json);
			}

		}
	}
}