﻿using System;
using System.Collections.Generic;
using System.Linq;
using GradAcademyLoader.Domain;

namespace GenerateTestData
{
	public class DataBuilder
	{
		private readonly GenerateRandomClient _clientGenerator;
		private readonly GenerateRandomCheck _checkGenerator;

		public DataBuilder(GenerateRandomClient clientGenerator, GenerateRandomCheck checkGenerator)
		{
			_clientGenerator = clientGenerator;
			_checkGenerator = checkGenerator;
		}

		public Data GenerateResults(int numberOfClients, int checksPerClient)
		{

			var clients = BuildClients(numberOfClients);

			var checks = BuildChecks(checksPerClient, clients.ToList());

			return new Data(clients, checks);
		}

		private IList<Client> BuildClients(int numberOfClients)
		{
			if (numberOfClients <= 0)
				throw new ArgumentException("If you want to generate client data you need to select more than 0 clients", "numberOfClients");

			var random = new Random();
			var clients = new List<Client>();

			for (var i = 0; i < numberOfClients; i++)
			{
				clients.Add(_clientGenerator.Generate(random.Next()));
			}

			return clients;
		}

		private IEnumerable<Check> BuildChecks(int checksPerClient, IList<Client> clients)
		{
			if (checksPerClient <= 0)
				throw new ArgumentException("Error cannot generate checks if you do not want any checks per client", "checksPerClient");
			if (!clients.Any())
				throw new ArgumentException("Need at least one client to generate checks", "clients");

			var checks = new List<Check>();

			foreach (var client in clients)
			{
				for (var i = 0; i < checksPerClient; i++)
				{
					checks.Add(_checkGenerator.Generate(client));
				}
			}

			return checks;
		}
	}
}