﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration;
using GradAcademyLoader.Domain;

namespace GenerateTestData
{
	public class GenerateCsvData
	{
		private readonly Data _data;

		public GenerateCsvData()
		{
			var dataBuilder = new DataBuilder(new GenerateRandomClient(), new GenerateRandomCheck());

			_data = dataBuilder.GenerateResults(20, 2);
		}

		public IEnumerable<string> GenerateCsvClients()
		{
			StringBuilder sb = new StringBuilder();
			StringWriter stringWriter = new StringWriter(sb);

			var csv = new CsvWriter(stringWriter);
			csv.WriteRecords(_data.Clients);
			var lines = sb.ToString().Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

			return lines.ToList();
		}

		public IEnumerable<string> GenerateCsvChecks()
		{
			StringBuilder sb = new StringBuilder();
			StringWriter stringWriter = new StringWriter(sb);

			var csv = new CsvWriter(stringWriter);
			csv.WriteRecords(_data.Checks);
			var lines = sb.ToString().Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

			return lines.ToList();
		}

		public string GenerateOneCsvClient()
		{
			StringBuilder sb = new StringBuilder();
			StringWriter stringWriter = new StringWriter(sb);
			var csvConfig = new CsvConfiguration {HasHeaderRecord = false};
			var csvWriter = new CsvWriter(stringWriter, csvConfig);
			csvWriter.WriteRecord(_data.Clients.First());
			return sb.ToString();
		}

		public string GenerateOneCsvCheck()
		{
			StringBuilder sb = new StringBuilder();
			StringWriter stringWriter = new StringWriter(sb);
			var csvConfig = new CsvConfiguration {HasHeaderRecord = false};
			var csvWriter = new CsvWriter(stringWriter, csvConfig);
			csvWriter.WriteRecord(_data.Checks.First());
			return sb.ToString();
		}

	}
}
