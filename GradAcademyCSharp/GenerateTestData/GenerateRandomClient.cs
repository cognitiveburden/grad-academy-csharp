﻿using System;
using System.Collections.Generic;
using GradAcademyLoader.Domain;

namespace GenerateTestData
{
	public class GenerateRandomClient
	{
		private readonly int _divisorToPickBadData;
		private List<Client> _clients;

		public GenerateRandomClient() : this(1) { }

		public GenerateRandomClient(int divisorToPickBadData)
		{
			_divisorToPickBadData = divisorToPickBadData;

			_clients = new List<Client>();
			for (var i = 0; i < 1000; i++)
			{
				if (i % _divisorToPickBadData != 0)
				{
					_clients.Add(new Client(Guid.Empty, String.Empty, States.NA));
				}

				else
				{
					_clients.Add(new Client(
						Guid.NewGuid(),
						Faker.Name.First(),
						(States)Enum.Parse(typeof(States), Faker.Address.UsStateAbbr())));
				}
			}
		}

		public Client Generate(int seed)
		{
			var r = new Random(seed);
			var index = r.Next(0, _clients.Count);

			return _clients[index];
		}
	}
}