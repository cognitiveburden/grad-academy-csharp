﻿using System.Collections.Generic;
using GradAcademyCSharp.BusinessObjects;
using GradAcademyCSharp.DataAccess;
using GradAcademyCSharp.DataAccess.Process;

namespace DatabaseLoader
{
    class Program
    {
        static void Main()
        {
            AutoMapperDataAccessConfiguration.Configure();

            var dataFactory = new DataFactory();

            var workerProcess = new WorkerDataAccess();
            var checkProcess = new CheckDataAccess();
            var clientProcess = new ClientDataAccess();
            var invoiceProcess = new InvoiceDataAccess();

            var workersWithIds = new List<Worker>();
            foreach (var worker in dataFactory.Workers)
            {
                worker.Id = 0;
                workersWithIds.Add(workerProcess.Add(worker));
            }


            var clientsWithIds = new List<Client>();
            var invoicesWithIds = new List<Invoice>();
            var checksWithIds = new List<Check>();

            foreach (var client in dataFactory.Clients)
            {
                client.Id = 0;
                var clientWIthId = clientProcess.Add(client);
                
                client.Id = clientWIthId.Id;
                clientsWithIds.Add(clientWIthId);
                      
                foreach (var invoice in dataFactory.GetInvoices(clientWIthId.Id))
                {
                    invoice.Id = 0;
                    invoicesWithIds.Add(invoiceProcess.Add(invoice));
                }
            }

            foreach (var check in dataFactory.Checks)
            {
                check.Id = 0;
                checksWithIds.Add(checkProcess.Add(check));
            }
        }
    }
}
