﻿using System.Collections.Generic;
using System.Linq;
using GradAcademyCSharp.BusinessObjects;
using GradAcademyCSharp.DataAccess.Process.Contracts;
using GradAcademyCSharp.Service.Contracts;

namespace GradAcademyCSharp.Service
{
    public class ClientService : IClientService
    {
        private readonly IFinancialDataAccess<Client> _clientDataAccess;

        public ClientService(IFinancialDataAccess<Client> clientDataAccess)
        {
            _clientDataAccess = clientDataAccess;
        }

        public Client GetClient(int clientId)
        {
            return _clientDataAccess.Get(clientId);
        }

        public List<Client> GetClients()
        {
            return _clientDataAccess.Get().ToList();
        }

        public Client UpdateClientInformation(int id, Client updatedClient)
        {
            return _clientDataAccess.Update(id, updatedClient);
        }
    }
}