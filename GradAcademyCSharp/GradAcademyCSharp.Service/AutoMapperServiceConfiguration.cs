﻿using AutoMapper;
using GradAcademyCSharp.DataAccess;

namespace GradAcademyCSharp.Service
{
    public class AutoMapperServiceConfiguration
    {
        public static void Configure()
        {
            AutoMapperDataAccessConfiguration.Configure();

            Mapper.AssertConfigurationIsValid();
        }
    }
}
