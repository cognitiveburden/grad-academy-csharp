using System;
using System.Collections.Generic;
using GradAcademyCSharp.BusinessObjects;

namespace GradAcademyCSharp.Service.Contracts
{
    public interface IWorkerService
    {
        Worker GetByEmail(string emailAddress);
        Worker GetWorker(int workerId);
        IList<Worker> GetContacts();
        bool SetProfileInformation(int workerId, Worker worker);
        Worker CreateWorker(Worker worker);

        IList<Check> GetOpenWorkerChecks(int workerId);
        IList<Check> GetMoreWorkerChecks(int checkCount, int workerId);

        TimeSpan GetAverageCheckCompletionTime(int workerId);
        int GetNumberOfChecksCompleted(int workerId, TimeSpan timespan);
    }
}