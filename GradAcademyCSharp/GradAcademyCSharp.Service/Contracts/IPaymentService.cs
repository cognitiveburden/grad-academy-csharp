﻿using System.Collections.Generic;
using GradAcademyCSharp.BusinessObjects;

namespace GradAcademyCSharp.Service.Contracts
{
    public interface IPaymentService
    {
        decimal SchedulePayment(int checkId, int invoiceId);
        Check GetCheck(int checkId);
        IEnumerable<Invoice> GetOpenInvoices(int clientId);
        Invoice GetInvoice(int invoiceId);

        List<PaidInvoice> GetClosedInvoices(int workerId);
    }
}
