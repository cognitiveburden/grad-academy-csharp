﻿using System;
using System.Collections.Generic;
using System.Linq;
using GradAcademyCSharp.BusinessObjects;
using GradAcademyCSharp.DataAccess.Process.Contracts;
using GradAcademyCSharp.Service.Contracts;

namespace GradAcademyCSharp.Service
{
    public class WorkerService : IWorkerService
    {
        private readonly IFinancialDataAccess<Worker> _workerDataAccess;
        private readonly IFinancialDataAccess<Check> _checkDataAccess;
        private readonly IFinancialDataAccess<Client> _clientDataAccess;

        public WorkerService(IFinancialDataAccess<Worker> workerDataAccess, IFinancialDataAccess<Check> checkDataAccess, IFinancialDataAccess<Client> clientDataAccess)
        {
            _workerDataAccess = workerDataAccess;
            _checkDataAccess = checkDataAccess;
            _clientDataAccess = clientDataAccess;
        }

        public Worker GetByEmail(string emailAddress)
        {
            return _workerDataAccess.Get().FirstOrDefault(
                x => x.EmailAddress.Equals(emailAddress, StringComparison.OrdinalIgnoreCase));
        }

        public Worker GetWorker(int workerId)
        {
            return _workerDataAccess.Get(workerId);
        }

        public IList<Worker> GetContacts()
        {
            return _workerDataAccess.Get().ToList();
        }

        public bool SetProfileInformation(int workerId, Worker worker)
        {
            return _workerDataAccess.Update(workerId, worker) != null;
        }

        public Worker CreateWorker(Worker worker)
        {
            return _workerDataAccess.Add(worker);
        }

        public IList<Check> GetOpenWorkerChecks(int workerId)
        {
            var checks = _checkDataAccess.Get()
                .Where(x => x.AssignedWorker.HasValue && x.AssignedWorker.Value == workerId && !x.Completed).ToList();

            foreach (var check in checks)
            {
                check.ClientName = _clientDataAccess.Get(check.ClientId).Name;
            }

            return checks;
        }

        public IList<Check> GetMoreWorkerChecks(int checkCount, int workerId)
        {
            var newChecksForWorker = _checkDataAccess.Get()
                .Where(x => !x.Completed && !x.AssignedWorker.HasValue)
                .Take(checkCount).ToList();


            foreach (var newCheckForWorker in newChecksForWorker)
            {
                newCheckForWorker.AssignedWorker = workerId;
                newCheckForWorker.StartDate = DateTime.Now;
                newCheckForWorker.ClientName = _clientDataAccess.Get(newCheckForWorker.ClientId).Name;

                _checkDataAccess.Update(newCheckForWorker.Id, newCheckForWorker);
            }

            return newChecksForWorker;
        }


        public TimeSpan GetAverageCheckCompletionTime(int workerId)
        {
            var completedChecks =
                _checkDataAccess.Get()
                    .Where(x => x.Completed && x.AssignedWorker.HasValue && x.AssignedWorker.Value == workerId && x.StartDate.HasValue && x.CompletedDate.HasValue && x.CompletedDate > x.StartDate).ToList();

            return completedChecks.Any()
                ? TimeSpan.FromMilliseconds(
                completedChecks.Select(x => x.CompletedDate.Value - x.StartDate.Value).Average(x => x.TotalMilliseconds)) : new TimeSpan();   
        }

        public int GetNumberOfChecksCompleted(int workerId, TimeSpan timespan)
        {
            return
                _checkDataAccess.Get().Count(x => x.Completed && x.AssignedWorker.HasValue && x.AssignedWorker.Value == workerId &&
                                                  x.CompletedDate >= (DateTime.Now - timespan));
        }
    }
}
