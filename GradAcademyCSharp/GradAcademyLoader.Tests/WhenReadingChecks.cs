﻿using System;
using System.Collections.Generic;
using System.Linq;
using GradAcademyLoader.Domain;
using GradAcademyLoader.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace GradAcademyLoader.Tests
{
	[TestClass]
	public class WhenReadingChecks
	{

		private ICheckDataProvider _checkDataProvider;
		private readonly string _knownCheckNumber = "1045";
		private readonly string _knownCheckAmount = "123.45";
		private readonly string _knownClientId = "44a7bee6-5207-4934-9622-e4a66117f8ed";
		private readonly string _knownHasImage = "true";
		private DataLoader _sut;

		[TestInitialize()]
		public void MyTestInitialize()
		{
			//var lineInformation = new LineInformation("testFileName", 1, 
			//	string.Format("{0},{1},{2},{3}", _knownCheckNumber, _knownCheckAmount,_knownClientId,_knownHasImage));
			//var knownCheck = new Check(_knownCheckNumber,);

			//_checkDataProvider = MockRepository.GenerateStub<ICheckDataProvider>();
			//_checkDataProvider.Stub(x => x.Read()).Return(
			//	new List<ClientLine>(new ClientLine()))
			//	//new List<string[]> { new[] { _knownCheckNumber, _knownCheckAmount, _knownClientId, _knownHasImage } });
			//_sut = new DataLoader(MockRepository.GenerateStub<IClientDataProvider>(), _checkDataProvider);
		}

		[TestCleanup()]
		public void MyTestCleanup()
		{
			_checkDataProvider = null;
			_sut = null;
		}

		[TestMethod]
		public void GivenAKnownCheckThenKnownClientIdReturned()
		{
			//var clients = _sut.ReadChecks();
			//Assert.AreEqual(clients.First().ClientId, Guid.Parse(_knownClientId));
		}

		//[TestMethod]
		//public void GivenAKnownCheckThenKnownNumberReturned()
		//{
		//	var clients = _sut.ReadChecks();
		//	Assert.AreEqual(clients.First().Number, Int32.Parse(_knownCheckNumber));
		//}

		//[TestMethod]
		//public void GivenAKnownCheckThenKnownAmountReturned()
		//{
		//	var clients = _sut.ReadChecks();
		//	Assert.AreEqual(clients.First().Amount, decimal.Parse(_knownCheckAmount));
		//}

		//[TestMethod]
		//public void GivenAKnownCheckThenKnownHasImageReturned()
		//{
		//	var clients = _sut.ReadChecks();
		//	Assert.AreEqual(clients.First().HasImage, bool.Parse(_knownHasImage));
		//}

	}
}
