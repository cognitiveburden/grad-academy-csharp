﻿using System;
using GradAcademyLoader.DataAccess;
using GradAcademyLoader.Domain;
using GradAcademyLoader.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GradAcademyLoader.Tests
{
	[TestClass]
	public class WhenParsingACheck
	{
		private Func<string, string, string, string, ParsedCheck> _sut;
		private const string ValidCheckNumber = "10001";
		private const string ValidAmount = "112.83";
		private const string ValidClientId = "0192f50d-871c-46f1-af02-2c757953e696";
		private const string ValidHasImage = "false";

		[TestInitialize]
		public void InitializeBeforeEachTest()
		{
			_sut = CheckParser.ParseCheck;
		}

		[TestCleanup]
		public void CleanUpAfterEachTest()
		{
			_sut = null;
		}

		[TestMethod]
		public void GivenValidStringFormatThenNoParsingErrors()
		{
			var result = ExecSut();
			Assert.AreEqual(string.Empty,result.ParsingErrors);
		}

		[TestMethod]
		public void GivenAlphaCheckNumberThenCheckNumberInParseErrors()
		{
			var result = ExecSut(checkNumber: "abc");
			StringAssert.Contains(result.ParsingErrors,"Check Number");
		}

		[TestMethod]
		public void GivenNegativeCheckNumberThenCheckNumberInParseErrors()
		{
			var result = ExecSut(checkNumber: "-123");
			StringAssert.Contains(result.ParsingErrors,"Check Number");
		}

		[TestMethod]
		public void GivenAlphaAmountThenAmountInParseErrors()
		{
			var result = ExecSut(amount: "33aba");
			StringAssert.Contains(result.ParsingErrors,"Amount");
		}

		[TestMethod]
		public void GivenNegativeAmountThenAmountInParseErrors()
		{
			var result = ExecSut(amount: "-33.23");
			StringAssert.Contains(result.ParsingErrors,"Amount");
		}

		[TestMethod]
		public void GivenInvalidClientIdThenClientIdInParseErrors()
		{
			var result = ExecSut(clientId: "3rwe3553");
			StringAssert.Contains(result.ParsingErrors, "Client Id");
		}

		[TestMethod]
		public void GivenEmptyClientIdThenClientIdInParseErrors()
		{
			var result = ExecSut(clientId: Guid.Empty.ToString());
			StringAssert.Contains(result.ParsingErrors, "Client Id");
		}

		[TestMethod]
		public void GivenNoHasImageValueThenNullHasImage()
		{
			var result = ExecSut(hasImage: string.Empty);
			Assert.IsNull(result.SourceCheck.HasImage);
		}

		[TestMethod]
		public void GivenHasValueTrueThenTrueHasImage()
		{
			var result = ExecSut(hasImage: "true");
			Assert.IsTrue(result.SourceCheck.HasImage.Value);
		}
		[TestMethod]
		public void GivenHasValueFalseThenFalseHasImage()
		{
			var result = ExecSut(hasImage: "false");
			Assert.IsFalse(result.SourceCheck.HasImage.Value);
		}


		private ParsedCheck ExecSut(string checkNumber = ValidCheckNumber,
			string amount = ValidAmount,
			string clientId = ValidClientId,
			string hasImage = ValidHasImage)
		{
			return _sut(checkNumber, amount, clientId, hasImage);
		}

		private string CreateCheckLine(
										string checkNumber = "10001",
										string amount = "112.83",
										string clientId = "0192f50d-871c-46f1-af02-2c757953e696",
										string hasImage = null)
		{
			return string.Format("{0},{1},{2},{3}",
				checkNumber,
				amount,
				clientId,
				hasImage ?? string.Empty);
		}
	}
}
