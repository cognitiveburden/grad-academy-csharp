using System;
using System.IO;
using System.Linq;
using GradAcademyLoader.DataAccess;
using GradAcademyLoader.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace GradAcademyLoader.Tests
{
	[TestClass]
	[DeploymentItem(@"Data\")]
	public class WhenLoadingCsvClientFile
	{
		private ILoaderConfig _fakeConfig;

		[TestInitialize]
		public void TestInitialize()
		{
			_fakeConfig = MockRepository.GenerateStub<ILoaderConfig>();
		}

		[TestCleanup]
		public void TestCleanUp()
		{
			_fakeConfig = null;
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void GivenAnInvalidFileNameThenException()
		{
			var sut = new ClientCsvDataProvider(new FakeBadConfig());
			sut.Read();
			Assert.Fail("Should Have thrown Exception");
		}

		[TestMethod]
		public void GivenAValidFileWithOneRecordThenSkipHeaderRow()
		{
			_fakeConfig.Stub(x => x.SourceDirectory).Return(AppDomain.CurrentDomain.BaseDirectory);
			_fakeConfig.Stub(x => x.CsvClientsFileName).Return("One_Client.csv");

			var sut = new ClientCsvDataProvider(_fakeConfig);
			var result = sut.Read();

			Assert.AreEqual(1, result.Count());
		}

		[TestMethod]
		public void GivenABadSeparatorFileWithOneRecordThenOnlyGetOneRow()
		{
			_fakeConfig.Stub(x => x.SourceDirectory).Return(AppDomain.CurrentDomain.BaseDirectory);
			_fakeConfig.Stub(x => x.CsvClientsFileName).Return("BadSeparator_Client.csv");

			var sut = new ClientCsvDataProvider(_fakeConfig);
			var result = sut.Read();

			StringAssert.Contains(result.First().Errors, "correct number of columns");
		}

		[TestMethod]
		public void GivenAValidFileWithOneRecordThenReturnThreeItems()
		{
			_fakeConfig.Stub(x => x.SourceDirectory).Return(AppDomain.CurrentDomain.BaseDirectory);
			_fakeConfig.Stub(x => x.CsvClientsFileName).Return("One_Client.csv");

			var sut = new ClientCsvDataProvider(_fakeConfig);
			var result = sut.Read();

			Assert.IsNotNull(result.First());
		}

		[TestMethod]
		[ExpectedException(typeof(ClientFileReadingException))]
		public void GivenFileLockedThenThrowClientFileReadingException()
		{
			var baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
			var fileName = "Locked_Client.csv";

			_fakeConfig.Stub(x => x.SourceDirectory).Return(baseDirectory);
			_fakeConfig.Stub(x => x.CsvClientsFileName).Return(fileName);

			var lockingPath = Path.Combine(baseDirectory, fileName);

			FileStream s = File.Open(lockingPath, FileMode.Open, FileAccess.ReadWrite);

			var sut = new ClientCsvDataProvider(_fakeConfig);
			var result = sut.Read();
			Assert.Fail("Should Have thrown client file reading exception;");
		}
	}
}