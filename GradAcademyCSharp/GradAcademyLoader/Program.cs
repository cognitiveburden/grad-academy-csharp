using System;
using System.Collections.Generic;
using System.Linq;
using GradAcademyLoader.DataAccess;
using GradAcademyLoader.Domain;
using GradAcademyLoader.Service;

namespace GradAcademyLoader
{
	public class Program
	{
		public static void Main(string[] args)
		{
			try
			{
				Run();
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
			}
			Console.ReadKey();
		}

		private static void Run()
		{
			var loaderConfig = new LoaderConfig();
			var csvClient = new ClientCsvDataProvider(loaderConfig);
			var csvCheck = new CheckCsvDataProvider(loaderConfig);

			var jsonClient = new ClientJsonDataProvider(loaderConfig);
			var jsonCheck = new CheckJsonDataProvider(loaderConfig);

			var jsonNetClient = new ClientJsonNetDataProvider(loaderConfig);
			var jsonNetCheck = new CheckJsonNetDataProvider(loaderConfig);


			//var dataLoaderCsv = new DataLoader(csvClient, csvCheck);
			//OutputClients(dataLoaderCsv);
			//OutputChecks(dataLoaderCsv);

			//var dataLoaderJson = new DataLoader(jsonClient, jsonCheck);
			//OutputClients(dataLoaderJson);
			//OutputChecks(dataLoaderJson);

			var dataLoaderJsonNet = new DataLoader(jsonNetClient, jsonNetCheck);
			OutputClients(dataLoaderJsonNet);
			OutputChecks(dataLoaderJsonNet);

		}

		private static void OutputClients(DataLoader dataLoader)
		{
			var clients = dataLoader.ReadClients().ToList();

			Console.WriteLine("Clients with Errors:");
			foreach (var client in clients.Where(c => c.HasErrors))
			{
				Console.WriteLine(client);
			}

			Console.WriteLine("No Error Clients:");
			foreach (var client in clients.Where(c => !c.HasErrors))
			{
				Console.WriteLine(client);
			}
			Console.WriteLine("Number of clients: {0}", clients.Count());
		}

		private static void OutputChecks(DataLoader dataLoader)
		{
			var checks = dataLoader.ReadChecks().ToList();
			Console.WriteLine("Checks with Errors:");
			foreach (var check in checks.Where(c => c.HasErrors))
			{
				Console.WriteLine(check);
			}

			Console.WriteLine("Checks with NO Errors:");
			foreach (var check in checks.Where(c => !c.HasErrors))
			{
				Console.WriteLine(check);
			}

			Console.WriteLine("Number of checks: {0}", checks.Count());
		}
	}

}
