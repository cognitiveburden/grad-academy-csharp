﻿using System;
using System.Collections.Generic;
using System.IO;
using CsvHelper;
using CsvHelper.Configuration;
using GradAcademyLoader.Domain;

namespace GradAcademyLoader.Service
{
	public class CsvFileWriter
	{
		private readonly ILoaderConfig _config;

		public CsvFileWriter(ILoaderConfig config)
		{
			_config = config;
		}

		public void WriteChecks(IEnumerable<Check> checks)
		{
			var filePath = PathToNewChecksFile();
			var directory = Path.GetDirectoryName(filePath);

			if (directory == null || !Directory.Exists(directory))
				throw new ArgumentException("Error the :" + directory + " does not exist.");

			WriteChecksToFile(filePath, checks);
		}

		public void WriteClients(IEnumerable<Client> clients)
		{
			var filePath = PathToNewClientsFile();
			var directory = Path.GetDirectoryName(filePath);

			if (directory == null || !Directory.Exists(directory))
				throw new ArgumentException("Error the :" + directory + " does not exist.");

			WriteClientsToFile(filePath, clients);
		}

		private void WriteChecksToFile(string filePath, IEnumerable<Check> checks)
		{
			using (var streamWriter = new StreamWriter(filePath, true))
			{
				var writer = new CsvWriter(streamWriter, new CsvConfiguration() { HasHeaderRecord = false });
				writer.WriteRecords(checks);
			}
		}

		private void WriteClientsToFile(string filePath, IEnumerable<Client> clients)
		{
			using (var streamWriter = new StreamWriter(filePath, true))
			{
				var writer = new CsvWriter(streamWriter, new CsvConfiguration() { HasHeaderRecord = false });
				writer.WriteRecords(clients);
			}
		}

		private string PathToNewClientsFile()
		{
			return Path.Combine(Path.GetTempPath(), _config.CsvClientsFileName);
		}

		private string PathToNewChecksFile()
		{
			return Path.Combine(Path.GetTempPath(), _config.CsvChecksFileName);
		}
	}
}
