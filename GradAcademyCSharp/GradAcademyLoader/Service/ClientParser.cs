using System;
using System.Text;
using System.Text.RegularExpressions;
using GradAcademyLoader.Domain;

namespace GradAcademyLoader.Service
{
	internal static class ClientParser
	{
		internal static ParsedClient ParseClient(string id, string name, string state)
		{
			var errorMessages = new StringBuilder();
			var tempClientId = Guid.Empty;
			var tempState = States.NA;
			var tempName = string.Empty;

			if (!IsValildId(id) || !Guid.TryParse(id, out tempClientId))
			{
				errorMessages.AppendLine(string.Format(" The Client Id'{0}' is not valid. ", id));
			}

			if (!IsValidName(name))
			{
				errorMessages.AppendLine(string.Format(" The Name for the client '{0}' is not a valid format. ", name));
			}
			else
			{
				tempName = name;
			}

			if (!IsValidState(state))
			{
				errorMessages.AppendLine(string.Format(" The State for the client '{0}' is not a valid format. ", name));
			}
			else
			{
				tempState = (States)Enum.Parse(typeof(States), state);
			}

			return new ParsedClient(new Client(tempClientId, tempName, tempState), errorMessages.ToString());
			}

		private static bool IsValidName(string name)
		{
			return !string.IsNullOrWhiteSpace(name) && Regex.IsMatch(name, @"^[a-z]+$", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
		}

		private static bool IsValidState(string state)
		{
			var validFormat =!string.IsNullOrWhiteSpace(state);
			var validValue = !States.NA.ToString().Equals(state);
			States tempState;
			var validState = Enum.TryParse(state, out tempState);
			return validFormat && validValue && validState;
		}

		private static bool IsValildId(string id)
		{
			bool guidIsEmpty = Guid.Empty.ToString().Equals(id);
			bool isValidIdPattern = Regex.IsMatch(id, @"\b[A-F0-9]{8}(?:-[A-F0-9]{4}){3}-[A-F0-9]{12}\b"
				, RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
			return !guidIsEmpty && isValidIdPattern;
		}
	}
}