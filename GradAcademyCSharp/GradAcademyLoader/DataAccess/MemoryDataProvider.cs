using System;
using System.Collections.Generic;
using Faker;
using GradAcademyLoader.Domain;
using GradAcademyLoader.Service;

namespace GradAcademyLoader.DataAccess
{
	public class MemoryDataProvider : ICheckDataProvider, IClientDataProvider
	{
		private readonly IList<ClientLine> _clients;

		private const int NumberOfClients = 10;

		public MemoryDataProvider()
		{
			_clients = CreateClients(NumberOfClients);
		}

		IEnumerable<CheckLine> ICheckDataProvider.Read()
		{
			return ReadChecksFromMemory();
		}

		string IClientDataProvider.Source { get { return "From Memory"; } }

		private IEnumerable<CheckLine> ReadChecksFromMemory()
		{
			var randomNum = RandomNumber.Next(10000);

			var checkLines = new List<CheckLine>();
			for (var i = 0; i < 20; i++)
			{
				var clientIndex = RandomNumber.Next(0, NumberOfClients);

				var check = new Check(
							RandomNumber.Next(10000), //check number
							RandomNumber.Next(100000) * 1.0m, //check value
							_clients[clientIndex].SourceClient.Id,
							RandomNumber.Next(1, 10) % 2 == 0);

				var lineInformation = new LineInformation("FromMemory", i, check.ToString(), DateTime.Now);

				var noErrors = string.Empty;

				checkLines.Add(new CheckLine(lineInformation, check, noErrors));
			}
			return checkLines;
		}

		IEnumerable<ClientLine> IClientDataProvider.Read()
		{
			return _clients;
		}

		string ICheckDataProvider.Source { get { return "From Memory"; } }

		private IList<ClientLine> CreateClients(int numberOfClients)
		{
			var clients = new List<ClientLine>();
			for (var i = 0; i < numberOfClients; i++)
			{
				var client = new Client(Guid.NewGuid(),
										Name.First(),
										(States)Enum.Parse(typeof(States), Address.UsStateAbbr()));

				var lineInformation = new LineInformation("From Memory", i, client.ToString(), DateTime.Now);

				var noErrors = string.Empty;

				clients.Add(new ClientLine(lineInformation, client, noErrors));
			}
			return clients;
		}
	}
}