using System;
using System.Collections.Generic;
using System.IO;
using CsvHelper;
using GradAcademyLoader.Domain;
using GradAcademyLoader.Service;

namespace GradAcademyLoader.DataAccess
{
	public class CheckCsvDataProvider : ICheckDataProvider
	{
		private readonly ILoaderConfig _config;

		public CheckCsvDataProvider(ILoaderConfig config)
		{
			_config = config;
		}

		public IEnumerable<CheckLine> Read()
		{
			var filePath = Source;

			if (!File.Exists(filePath))
				throw new ArgumentException("Error the file path:" + filePath + " does not exist.");

			var checkLines =  ReadChecksFromFile(filePath); ;
			//TODO: Ensure the client exists
			return checkLines;
		}

		private static IEnumerable<CheckLine> ReadChecksFromFile(string filePath)
		{
			var checkLines = new List<CheckLine>();
			try
			{
				using (var textReader = File.OpenText(filePath))
				{
					using (var parser = new CsvParser(textReader))
					{
						parser.Read(); //Read Header
						while (true)
						{
							var row = parser.Read();
							if (row == null)
								break;
							var lineInfo = new LineInformation(filePath, parser.RawRow, parser.RawRecord, DateTime.Now);

							var tempCheck =
								row.Length != 4
								? new ParsedCheck(new Check(0,0.0m,Guid.Empty,null), "Error the line was not parsed into correct number of columns.")
								: CheckParser.ParseCheck(row[0], row[1], row[2], row[3]);

							checkLines.Add(new CheckLine(lineInfo, tempCheck.SourceCheck, tempCheck.ParsingErrors));

						}
					}
				}
			}
			catch (IOException io)
			{
				throw new CheckFileReadingException("Error When Reading Check File.", io);
			}

			return checkLines;
		}

		public string Source { get { return Path.Combine(_config.SourceDirectory, _config.CsvChecksFileName); } }
	}
}