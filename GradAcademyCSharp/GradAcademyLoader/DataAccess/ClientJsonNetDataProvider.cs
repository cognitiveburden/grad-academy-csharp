﻿using System;
using System.Collections.Generic;
using System.IO;
using GradAcademyLoader.Domain;
using GradAcademyLoader.Service;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace GradAcademyLoader.DataAccess
{
	public class ClientJsonNetDataProvider : IClientDataProvider
	{
		private readonly ILoaderConfig _loaderConfig;

		public ClientJsonNetDataProvider(ILoaderConfig loaderConfig)
		{
			_loaderConfig = loaderConfig;
		}

		public IEnumerable<ClientLine> Read()
		{
			if (!File.Exists(Source))
				throw new ArgumentException("Error could not find Client Json file at: " + Source);

			var clients = ReadClientsFromJsonFile(Source);

			return clients;
		}

		private IEnumerable<ClientLine> ReadClientsFromJsonFile(string filePath)
		{
			var clientLines = new List<ClientLine>();

			using (var reader = File.OpenText(filePath))
			{
				using (var r = new JsonTextReader(reader))
				{
					while (r.Read())
					{
						if (r.TokenType != JsonToken.StartObject) continue;

						var jsonObject = JObject.Load(r);

						var rawJsonString = jsonObject.ToString();
						var lineNumber = r.LineNumber;
						var error = string.Empty;

						var client = JsonConvert.DeserializeObject<JsonClient>(rawJsonString,
							new JsonSerializerSettings()
							{
								Error = (sender, args) =>
								{
									error = args.ErrorContext.Error.Message;
									args.ErrorContext.Handled = true;
								}
							});

						var lineInfo = new LineInformation(filePath, lineNumber, rawJsonString);
						if (client == null)
						{
							clientLines.Add(new ClientLine(lineInfo, null, error));
						}
						else
						{
							var parsedClient = ClientParser.ParseClient(client.Id, client.Name, client.State);
							clientLines.Add(new ClientLine(lineInfo, parsedClient, parsedClient.ParsingErrors + error));
						}
					}
				}
				return clientLines;
			}


		}

		public string Source { get { return Path.Combine(_loaderConfig.SourceDirectory, _loaderConfig.JsonClientsFileName); } }


		private class JsonClient
		{
			public string Id { get; set; }
			public string Name { get; set; }
			public string State { get; set; }
		}

	}
}