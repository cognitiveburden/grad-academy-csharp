﻿namespace GradAcademyLoader.Domain
{
	public class ParsedCheck
	{
		public Check SourceCheck { get; private set; }
		public string ParsingErrors { get; private set; }

		public ParsedCheck(Check sourceCheck, string parsingErrors)
		{
			SourceCheck = sourceCheck;
			ParsingErrors = parsingErrors;
		}
	}
}
