﻿using System.IO;

namespace GradAcademyLoader.Domain
{
	public class LoaderConfig : ILoaderConfig
	{
		public string SourceDirectory { get { return Path.GetTempPath(); } }
		public string CsvClientsFileName { get { return "GradAcademy_Clients.csv"; } }
		public string CsvChecksFileName { get { return "GradAcademy_Checks.csv"; } }

		public string JsonClientsFileName { get { return "GradAcademy_Clients.json"; } }
		public string JsonChecksFileName { get { return "GradAcademy_Checks.json"; } }
	}

	public class PersistanceConfig
	{
		public string CsvClientsOutputFileName { get { return "GradAcademy_Output_Clients.csv"; } }
		public string CsvChecksOutputFileName { get { return "GradAcademy_Output_Checks.csv"; } }

		public string JsonClientsOutputFileName { get { return "GradAcademy_Output_Clients.json"; } }
		public string JsonChecksOutputFileName { get { return "GradAcademy_Output_Checks.json"; } }
	}
}
