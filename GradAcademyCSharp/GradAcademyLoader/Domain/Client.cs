using System;
using System.Runtime.Serialization;

namespace GradAcademyLoader.Domain
{
	[DataContract]
	public class Client
	{
		public Client(Guid id, string name, States state)
		{
			Id = id;
			Name = name;
			State = state;
		}

		[DataMember]
		public Guid Id { get; private set; }
		[DataMember]
		public string Name { get; private set; }
		[DataMember]
		public States State { get; private set; }

		public override string ToString()
		{
			return "Client Id: " + Id + " Name: " + Name;
			//TODO: Make this simpler to allow new grads to implement better tostring
			//return string.Format(
			//	"Client with Id: '{0}';{1}Name: '{2}';{3}State: '{4}'; {5}",
			//	Id,
			//	Environment.NewLine,
			//	Name,
			//	Environment.NewLine,
			//	State,
			//	Environment.NewLine);
		}
	}
}