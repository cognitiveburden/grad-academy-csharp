using System.Text;

namespace GradAcademyLoader.Domain
{
	public class ClientLine
	{
		public LineInformation LineInformation { get; set; }
		public Client SourceClient { get; set; }
		public string Errors { get; set; }
		public bool HasErrors { get { return SourceClient == null || !string.IsNullOrWhiteSpace(Errors); } }

		public ClientLine(LineInformation lineInformation, Client sourceClient, string errors)
		{
			LineInformation = lineInformation;
			SourceClient = sourceClient;
			Errors = errors;
		}

		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();
			if (HasErrors)
			{
				sb.Append(LineInformation.ToString());
				sb.Append(Errors);
			}
			else
			{
				sb.AppendFormat("Client:{0}", SourceClient.ToString());
			}
			return sb.ToString();
		}
	}
}