using System.Text;

namespace GradAcademyLoader.Domain
{
	public class CheckLine
	{
		public LineInformation LineInformation { get; set; }
		public Check SourceCheck { get; set; }
		public string Errors { get; set; }
		public bool HasErrors { get { return SourceCheck == null || !string.IsNullOrWhiteSpace(Errors); } }

		public CheckLine(LineInformation lineInformation, Check sourceCheck, string errors)
		{
			LineInformation = lineInformation;
			SourceCheck = sourceCheck;
			Errors = errors;
		}

		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();
			if (HasErrors)
			{
				sb.Append(LineInformation.ToString());
				foreach (var error in Errors)
				{
					sb.Append(error);
				}
			}
			else
			{
				sb.AppendFormat("Check:{0}", SourceCheck.ToString());
			}
			return sb.ToString();
		}
	}
}