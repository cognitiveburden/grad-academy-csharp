﻿namespace GradAcademyLoader.Domain
{
	public interface ILoaderConfig
	{
		string SourceDirectory { get; }
		string CsvClientsFileName { get; }
		string CsvChecksFileName { get; }
		string JsonClientsFileName { get; }
		string JsonChecksFileName { get; }
	}
}