﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using GradAcademyLoader;
using GradAcademyLoader.Domain;
using GradAcademyLoader.Service;

namespace ClientCheckApi_Lab1
{
	public class ClientApi
	{
		private MemoryLoader _memoryLoader;
		public ClientApi()
		{
			_memoryLoader = new MemoryLoader();
		}

		public Client GetFirstClient()
		{
	
			return _memoryLoader.ReadClients()
		}
	}
}
