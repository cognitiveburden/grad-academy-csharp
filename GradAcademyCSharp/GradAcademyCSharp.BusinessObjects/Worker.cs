﻿namespace GradAcademyCSharp.BusinessObjects
{
    public class Worker
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string JobTitle { get; set; }

        public string EmailAddress { get; set; }

    }
}
