﻿using System;

namespace GradAcademyCSharp.BusinessObjects
{
    public class PaidInvoice
    {
        public int Id { get; set; }

        public decimal AmountPaid { get; set; }

        public int InvoiceId { get; set; }

        public int WorkerId { get; set; }

        public decimal AmountDueBefore { get; set; }

        public DateTime DueDate { get; set; }

        public DateTime CompletedDate { get; set; }
    }
}
